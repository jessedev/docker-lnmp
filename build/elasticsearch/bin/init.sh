#!/bin/bash

# allow the container to be started with `--user`
if [[ "$1" == elasticsearch* ]] && [ "$(id -u)" = '0' ]; then
	if [ "$1" = 'elasticsearch' ]; then
		find "$ELASTICSEARCH_DIR" \! -user jesse -exec chown jesse:jesse '{}' +
	fi
	exec su -c "$@" -s /bin/bash jesse
fi

# 进入交互式终端
exec "$@"