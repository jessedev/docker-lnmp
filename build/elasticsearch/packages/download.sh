#!/bin/bash
set -ex

# 版本和系统架构
version=7.14.2
arch=$(uname -m)
es_dir=/usr/local/app/elasticsearch

# 下载
current_dir="$(dirname "$(readlink -f "$0")")"
# See https://www.elastic.co/downloads/past-releases/elasticsearch-7-14-2
wget -P "$current_dir" "https://artifacts.elastic.co/downloads/elasticsearch/elasticsearch-${version}-linux-${arch}.tar.gz"

# Set https://github.com/medcl/elasticsearch-analysis-ik/releases/tag/v7.14.2
wget -P "$current_dir" "https://github.com/medcl/elasticsearch-analysis-ik/releases/download/v${version}/elasticsearch-analysis-ik-${version}.zip"

mkdir -p "$es_dir"
tar -zxf "$current_dir/elasticsearch-${version}-linux-${arch}.tar.gz" -C "$es_dir" --strip-components=1

mkdir -p "$es_dir/plugins/analysis-ik"
mv "$current_dir/elasticsearch-analysis-ik-${version}.zip" "$es_dir/plugins/analysis-ik"
cd "$es_dir/plugins/analysis-ik"
unzip "elasticsearch-analysis-ik-${version}.zip" && rm -rf "$es_dir/plugins/analysis-ik/elasticsearch-analysis-ik-${version}.zip"
