#!/bin/bash
# shellcheck disable=SC2164
# 安装 gcc8
set -ex
yum install -y centos-release-scl
rpm --import /etc/pki/rpm-gpg/RPM-GPG-KEY-CentOS-SIG-SCLo
yum install -y devtoolset-9-gcc-c++
source /opt/rh/devtoolset-9/enable

# 创建用户
groupadd -r jesse \
    && useradd -r -s /sbin/nologin \
    -d /usr/local/app \
    -u 1000  -g jesse -m jesse

# 编译安装
cd /tmp/packages
tar -zxf mysql-boost-8.0.16.tar.gz
cd mysql-8.0.16
mkdir build && cd build
cmake3 .. \
       -DCMAKE_INSTALL_PREFIX=/usr/local/app/mysql \
       -DMYSQL_DATADIR=/usr/local/app/mysql/data \
       -DSYSCONFDIR=/usr/local/app/mysql/etc \
       -DWITH_MYISAM_STORAGE_ENGINE=1 \
       -DWITH_INNOBASE_STORAGE_ENGINE=1 \
       -DWITH_FEDERATED_STORAGE_ENGINE=1 \
       -DWITH_BLACKHOLE_STORAGE_ENGINE=1 \
       -DWITH_ARCHIVE_STORAGE_ENGINE=1 \
       -DWITHOUT_EXAMPLE_STORAGE_ENGINE=1 \
       -DWITH_PERFSCHEMA_STORAGE_ENGINE=1 \
       -DENABLED_LOCAL_INFILE=1 \
       -DWITH_BOOST=/tmp/packages/mysql-8.0.16/boost \
       -DWITH_SSL=system \
       -DWITH_ZLIB=system \
       -DWITH_LZ4=system \
       -DWITH_EDITLINE=bundled \
       -DWITH_ZLIB=bundled \
       -DWITH_SYSTEMD=1 \
       -DDEFAULT_CHARSET=utf8mb4 \
       -DDEFAULT_COLLATION=utf8mb4_general_ci \
       -DWITH_EXTRA_CHARSETS=all \
       -DWITH_UNIT_TESTS=OFF
make -j4 && make install

# 清理依赖
rm -rf /tmp/* \
    && yum remove -y cmake3 centos-release-scl devtoolset-9-gcc-c++\
    && yum clean all \
    && rm -rf /var/cache/yum/*