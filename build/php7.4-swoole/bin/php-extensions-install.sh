#!/bin/bash
set -ex
# GCC 9
source /opt/rh/devtoolset-9/enable

extension_dir="/tmp/extensions"
php_dir="/usr/local/app/php7.4"
php_bin_dir="$php_dir/bin"
php_confd_dir="$php_dir/etc/conf.d"

declare -A extensions=(
  ["phpredis-5.3.7.tar.gz"]="redis"
  ["LZF-1.7.0.tgz"]="lzf"
)

compile_and_install_extension() {
  extension_name=$1
  ini_file=$2
  echo "==========================Install PHP Extensions: $extension_name======================="
  cd "$extension_name"
  "$php_bin_dir/phpize"
  ./configure --with-php-config="$php_bin_dir/php-config"
  make && make install

  echo "extension=$extension_name.so" > "$php_confd_dir/$ini_file"
}

echo "==========================编译和安装PHP扩展======================="

for tar_file in "$extension_dir"/*.{tgz,gz}; do
  file_path=$(dirname "$tar_file")
  file_name=$(basename "$tar_file")
  # 包名 => 扩展命名
  if [[ -n "${extensions[$file_name]}" ]]; then
    extension_name="${extensions[$file_name]}"
  else
    # 没定义走默认
    extension_name=$(echo "$file_name" | sed 's/-.*$//')
  fi

  ini_file="php-ext-$(echo "$extension_name" | tr '-' '_').ini"
  cd "$file_path"
  mkdir -p "$extension_name"
  tar -zxf "$file_name" -C "$extension_name" --strip-components=1

  compile_and_install_extension "$extension_name" "$ini_file"
done

echo "==========================编译和安装完成======================="
