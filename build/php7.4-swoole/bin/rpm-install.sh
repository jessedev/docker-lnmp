#!/bin/bash
set -ex
# shellcheck disable=SC2164

#echo "==========================make======================="
#cd /tmp/rpm
#tar -zxf make-4.2.tar.gz
#cd  make-4.2
#./configure  && make && make install
#ln -s -f /usr/local/bin/make /usr/bin/make

#echo "==========================CMake======================="
#cd /tmp/rpm
#tar -zxf CMake-3.25.3.tar.gz
#cd  CMake-3.25.3
#./bootstrap && make -j4 && make install -j4

echo "==========================libzip======================="
cd /tmp/rpm
tar -zxf libzip-1.9.2.tar.gz
cd libzip-1.9.2
mkdir build && cd build && cmake3 -DCMAKE_INSTALL_PREFIX=/usr .. && make && make install

echo "==========================oniguruma======================="
cd /tmp/rpm
tar -zxf oniguruma-6.9.8.tar.gz
cd oniguruma-6.9.8
./autogen.sh && ./configure --prefix=/usr --libdir=/lib64 && make && make install
