#!/bin/bash
set -ex
# shellcheck disable=SC2164
php_dir="/usr/local/app/php7.4"
php_bin_dir="$php_dir/bin"
php_confd_dir="$php_dir/etc/conf.d"

cd /tmp/
wget https://pecl.php.net/get/swoole-4.8.12.tgz
tar -zxf swoole-4.8.12.tgz
cd swoole-4.8.12
"$php_bin_dir/phpize"

./configure --with-php-config="$php_bin_dir/php-config"  --enable-openssl --enable-http2 --enable-swoole-curl --enable-swoole-json
make && make install
echo "extension=swoole.so" > "$php_confd_dir/php-ext-swoole.ini"
echo "swoole.use_shortname = 'Off'" >> "$php_confd_dir/php-ext-swoole.ini"
echo "opcache.enable_cli = 'On'" >> "${php_confd_dir}/php-ext-opcache.ini"