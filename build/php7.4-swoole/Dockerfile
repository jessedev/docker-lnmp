ARG VERSION=7.9.2009
FROM centos:${VERSION}

LABEL Maintainer="Jesse <jessedev@163.com>"
LABEL Description="PHP7.4"

ENV TZ=Asia/Shanghai
ENV LANG=en_US.UTF-8 \
    LANGUAGE=en_US:en \
    LC_ALL=en_US.UTF-8 \
    PATH="/usr/local/app/php7.4/bin:$PATH"

# 设置时区
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

# 安装包
COPY ./packages /tmp/packages
COPY ./rpm /tmp/rpm
COPY ./extensions /tmp/extensions
COPY ./bin /tmp/bin

# 安装
RUN set -ex \
    && rpm --import /etc/pki/rpm-gpg/RPM-GPG-KEY* \
    && yum install -y epel-release \
    # 阿里镜像
    && curl -o /etc/yum.repos.d/epel.repo http://mirrors.aliyun.com/repo/epel-7.repo \
    # 解决日期报错
    && yum install -y glibc-common \
    && localedef -c -i en_US -f UTF-8 en_US.UTF-8 \
    && yum clean all \
    && yum makecache \
    # 安装依赖
    && yum update -y \
    && yum install -y gcc gcc-c++ cmake3 automake libtool autoconf \
       libc-client-devel libxml2 libxml2-devel openssl openssl-devel  \
       bzip2 bzip2-devel libcurl libcurl-devel libjpeg libjpeg-devel \
       libpng libpng-devel freetype freetype-devel gmp gmp-devel libmcrypt \
       readline readline-devel libxslt libxslt-devel zlib-devel glib2 \
       glib2-devel curl gdbm-devel db4-devel libXpm-devel libX11-devel gd gmp-devel \
       expat-devel xmlrpc-c icu libicu-devel  libmcrypt libmemcached \
       sqlite-devel postgresql-devel gmp libsodium-devel pcre libevent-devel wget \
    # Gcc 9
    && yum install -y centos-release-scl \
    && rpm --import /etc/pki/rpm-gpg/RPM-GPG-KEY-CentOS-SIG-SCLo \
    && yum install -y devtoolset-9-gcc devtoolset-9-gcc-c++ \
    && yum clean all \
    && rm -rf /var/cache/yum/* \
    # 安装rpm
    && chmod +x /tmp/bin/rpm-install.sh && sh /tmp/bin/rpm-install.sh && rm -rf /tmp/rpm \
    # 安装php
    && chmod +x /tmp/bin/php-install.sh && sh /tmp/bin/php-install.sh && rm -rf /tmp/packages \
    #  安装php扩展
    && chmod +x /tmp/bin/php-extensions-install.sh && sh /tmp/bin/php-extensions-install.sh && rm -rf /tmp/extensions \
    #  安装swoole
    && chmod +x /tmp/bin/php-swoole-install.sh && sh /tmp/bin/php-swoole-install.sh \
    ## 清理依赖
    && yum remove -y centos-release-scl devtoolset-9-gcc devtoolset-9-gcc-c++ && rm -rf /tmp/* && rm -rf /var/cache/*

WORKDIR /www

CMD ["/usr/sbin/init"]