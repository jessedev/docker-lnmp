#!/bin/bash
set -ex
# shellcheck disable=SC2164
# GCC 9
source /opt/rh/devtoolset-9/enable

# 路径变量
php_install_dir="/usr/local/app/php7.4"
php_config_dir="${php_install_dir}/etc"
php_fpm_dir="${php_config_dir}/php-fpm.d"
tmp_dir="/tmp/packages"
php_package_name="php-7.4.33.tar.gz"

# 创建用户
groupadd -r jesse \
  && useradd -r -s /sbin/nologin \
  -d /usr/local/app \
  -u 1000  -g jesse -m jesse

echo "=============PHP INSTALL============="
# shellcheck disable=SC2164
mkdir -p "$tmp_dir/php"
tar -zxf "$tmp_dir/$php_package_name" --strip-components=1  -C "$tmp_dir/php"

cd "$tmp_dir/php"
# 编译安装
./configure --prefix="${php_install_dir}" \
--with-config-file-path="${php_config_dir}" \
--with-config-file-scan-dir="${php_config_dir}/conf.d" \
--with-fpm-user=jesse \
--with-fpm-group=jesse \
--with-curl \
--with-gettext \
--with-iconv-dir \
--with-kerberos \
--with-libdir=lib64 \
--with-libxml \
--with-mysqli \
--with-openssl \
--with-pdo-mysql \
--with-pdo-mysql=mysqlnd \
--with-pdo-sqlite \
--with-pdo-pgsql \
--with-xmlrpc \
--with-xsl \
--with-bz2 \
--with-mhash \
--with-zlib \
--with-freetype \
--with-jpeg \
--with-gmp \
--with-imap \
--with-sodium \
--with-imap-ssl \
--with-zip \
--with-readline \
--enable-gd \
--enable-bcmath \
--enable-mbregex \
--enable-pcntl \
--enable-shmop \
--enable-soap \
--enable-sockets \
--enable-sysvmsg \
--enable-sysvsem \
--enable-sysvshm \
--enable-phar \
--enable-fpm \
--enable-mysqlnd \
--enable-inline-optimization \
--enable-ftp \
--enable-xml \
--enable-calendar \
--enable-ctype \
--enable-gd-jis-conv \
--enable-mbstring \
--enable-opcache \
--enable-intl  \
--disable-debug \
--disable-rpath \
--disable-phpdbg \
--disable-dtrace

make -j4 && make install -j4

mkdir -p "${php_config_dir}/conf.d"

# 环境变量
#echo 'export PATH="/usr/local/app/php7.4/bin:$PATH"' >> ~/.bashrc && source ~/.bashrc

# 配置相关
cp "${php_config_dir}/php-fpm.conf.default" "${php_config_dir}/php-fpm.conf"
cp "${php_fpm_dir}/www.conf.default" "${php_fpm_dir}/www.conf"

# fpm命令
cp "$tmp_dir/php/sapi/fpm/init.d.php-fpm" "${php_install_dir}/bin/php-fpm"
chmod +x "${php_install_dir}/bin/php-fpm"

# 启用opcache扩展
echo 'zend_extension=opcache.so' > "${php_config_dir}/conf.d/php-ext-opcache.ini"