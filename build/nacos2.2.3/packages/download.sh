#!/bin/bash
# See https://downloads.mysql.com/archives/community/
current_dir="$(dirname "$(readlink -f "$0")")"
wget -P "$current_dir" https://github.com/alibaba/nacos/releases/download/2.2.3/nacos-server-2.2.3.tar.gz
