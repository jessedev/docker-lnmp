#!/bin/bash

# allow the container to be started with `--user`
if [[ "$1" == kibana* ]] && [ "$(id -u)" = '0' ]; then
	if [ "$1" = 'kibana' ]; then
		find "$KIBANA_DIR" \! -user jesse -exec chown jesse:jesse '{}' +
	fi
	exec su -c "$@" -s /bin/bash jesse
fi

# 进入交互式终端
exec "$@"