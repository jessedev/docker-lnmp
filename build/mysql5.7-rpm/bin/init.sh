#!/bin/bash

directory="/usr/local/app/mysql/data"

# 如果空目录，就初始化数据
if [ -z "$(ls -A $directory)" ]; then
    chown jesse:jesse "$directory" /usr/local/app/mysql/run /var/log/mysql
    mysqld --initialize-insecure \
        --user=jesse \
        --default-time-zone=SYSTEM \
        --datadir="$directory" 2>&1 | tee /var/log/mysql-init.log
fi

# 进入交互式终端
exec "$@"