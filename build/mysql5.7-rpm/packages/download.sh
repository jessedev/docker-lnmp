#!/bin/bash
# See https://downloads.mysql.com/archives/community/
current_dir="$(dirname "$(readlink -f "$0")")"
wget -P "$current_dir" https://downloads.mysql.com/archives/get/p/23/file/mysql-community-server-5.7.41-1.el7.x86_64.rpm
wget -P "$current_dir" https://cdn.mysql.com/archives/mysql-5.7/mysql-community-libs-5.7.41-1.el7.x86_64.rpm
wget -P "$current_dir" https://cdn.mysql.com/archives/mysql-5.7/mysql-community-common-5.7.41-1.el7.x86_64.rpm
wget -P "$current_dir" https://cdn.mysql.com/archives/mysql-5.7/mysql-community-client-5.7.41-1.el7.x86_64.rpm