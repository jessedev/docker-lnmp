#!/bin/bash
# See https://downloads.mysql.com/archives/community/
current_dir="$(dirname "$(readlink -f "$0")")"
wget -P "$current_dir" https://downloads.mysql.com/archives/get/p/23/file/mysql-5.7.41-linux-glibc2.12-x86_64.tar.gz
