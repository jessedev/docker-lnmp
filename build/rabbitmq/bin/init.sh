#!/bin/bash


# allow the container to be started with `--user`
if [[ "$1" == rabbitmq* ]] && [ "$(id -u)" = '0' ]; then
	if [ "$1" = 'rabbitmq-server' ]; then
		find "$RABBITMQ_MNESIA_BASE" \! -user rabbitmq -exec chown rabbitmq:rabbitmq '{}' +
		find "$RABBITMQ_LOG_BASE" \! -user rabbitmq -exec chown rabbitmq:rabbitmq '{}' +
	fi
	exec su -c "$@" -s /bin/bash rabbitmq
fi

# 进入交互式终端
exec "$@"