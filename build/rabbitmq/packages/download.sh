#!/bin/bash
# See https://packagecloud.io/rabbitmq/erlang?page=3
current_dir="$(dirname "$(readlink -f "$0")")"
wget -O "$current_dir/erlang-23.3.4.11-1.el7.x86_64.rpm"  --content-disposition "https://packagecloud.io/rabbitmq/erlang/packages/el/7/erlang-23.3.4.11-1.el7.x86_64.rpm/download.rpm?distro_version_id=140"
# See https://github.com/rabbitmq/rabbitmq-server/releases/tag/v3.9.16
wget -P "$current_dir" https://github.com/rabbitmq/rabbitmq-server/releases/download/v3.9.16/rabbitmq-server-3.9.16-1.el7.noarch.rpm