#!/bin/bash
current_dir="$(dirname "$(readlink -f "$0")")"
# See https://repo.mongodb.org/yum/redhat/7/mongodb-org/4.4/x86_64/RPMS/
wget -P "$current_dir" https://repo.mongodb.org/yum/redhat/7/mongodb-org/4.4/x86_64/RPMS/mongodb-org-server-4.4.9-1.el7.x86_64.rpm
wget -P "$current_dir" https://repo.mongodb.org/yum/redhat/7/mongodb-org/4.4/x86_64/RPMS/mongodb-org-mongos-4.4.9-1.el7.x86_64.rpm
wget -P "$current_dir" https://repo.mongodb.org/yum/redhat/7/mongodb-org/4.4/x86_64/RPMS/mongodb-org-shell-4.4.9-1.el7.x86_64.rpm
wget -P "$current_dir" https://repo.mongodb.org/yum/redhat/7/mongodb-org/4.4/x86_64/RPMS/mongodb-org-database-tools-extra-4.4.9-1.el7.x86_64.rpm
wget -P "$current_dir" https://repo.mongodb.org/yum/redhat/7/mongodb-org/4.4/x86_64/RPMS/mongodb-database-tools-100.7.2.x86_64.rpm
wget -P "$current_dir" https://repo.mongodb.org/yum/redhat/7/mongodb-org/4.4/x86_64/RPMS/mongodb-org-tools-4.4.9-1.el7.x86_64.rpm