#### Usage

#### Usage

##### 1. Navigate to the project

    cd docker-lnmp

##### 2. Environment variables and configuration
    
    # Environment variables
    cp .env.example .env
    # Automatically pull images from Alibaba Private Registry
    cp ./docker-compose-example.yml ./docker-compose.yml

##### 3. Build images and run containers

    # Build services
    docker-compose up -d

##### 4. If manual service build is required
    
    # Download the Nacos installation package in advance or manually download and place it in the build/nacos/packages directory
    sh build/nacos/packages/download.sh
    # Download the MySQL package in advance as it is too large to upload
    sh build/mysql/packages/download.sh
    # If you want to build the image locally
    cp ./docker-compose-build.yml ./docker-compose.yml
    docker-compose up -d

##### 5. Modify the root password for the MySQL database

    -- Modify the root user for local connection
    ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY 'a123456';
    GRANT ALL PRIVILEGES ON *.* TO 'root'@'locahost';
    -- Create a new root user for external connections
    CREATE USER 'root'@'%' IDENTIFIED WITH mysql_native_password BY 'a123456';
    GRANT ALL PRIVILEGES ON *.* TO 'root'@'%';
    -- Flush privileges
    FLUSH PRIVILEGES;

##### 6. Create an admin account for MongoDB

    # Enter the container
    docker exec -it jesse-mongodb-service mongo admin
    # Create an admin user
    db.createUser({ user:'root',pwd:'a123456',roles:[ { role:'userAdminAnyDatabase', db: 'admin'},'readWriteAnyDatabase']});
    
    # Validate the password
    db.auth("root", "a123456");
    
    # Test MongoDB login
    mongo --authenticationDatabase admin -u root -p a123456

##### 7. RabbitMQ related information

###### 7.1 Access the web page

    http://localhost:15672/#/

###### 7.2 Account credentials

    The default account credentials can be found in the configuration or can be modified. Default: guest guest

###### 7.3 Create a new administrator
    
    rabbitmqctl add_user admin a123456
    rabbitmqctl set_permissions -p / admin ".*" ".*" ".*"
    rabbitmqctl set_user_tags admin administrator

##### 8. Nacos

    http://localhost:8848/nacos/index.html

##### 9. Elasticsearch
    
    http://localhost:9200/





