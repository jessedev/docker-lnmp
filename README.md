####  使用方式

##### 1.进入项目

      cd docker-lnmp

##### 2.环境变量配置等

    # 环境变量
    cp .env.example .env
    # 自动拉取阿里私服镜像
    cp ./docker-compose-example.yml ./docker-compose.yml

##### 3.构建镜像及运行容器

    # 构建服务
    docker-compose up -d

##### 4.如果需要手动构建服务
    
    # 提前下载nacos安装包或者手动下载放在build/nacos/packages目录下
    sh build/nacos/packages/download.sh
    # 提前下载mysql包，因为mysql太大无法上传
    sh build/mysql/packages/download.sh
    # 如果想本机构建镜像
    cp ./docker-compose-build.yml ./docker-compose.yml
    docker-compose up -d

##### 5.修改Msql数据库root用户密码
    
    -- 修改本地连接root用户
    ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY 'a123456';
    GRANT ALL PRIVILEGES ON *.* TO 'root'@'locahost';
    -- 新建一个外部root用户
    CREATE USER 'root'@'%' IDENTIFIED WITH mysql_native_password BY 'a123456';
    GRANT ALL PRIVILEGES ON *.* TO 'root'@'%';
    -- 刷新权限
    FLUSH PRIVILEGES;

##### 6.MongoDB创建管理员账号

    # 进入容器
    docker exec -it jesse-mongodb-service mongo admin
    # 创建管理员
    db.createUser({ user:'root',pwd:'a123456',roles:[ { role:'userAdminAnyDatabase', db: 'admin'},'readWriteAnyDatabase']});

    # 校验密码
    db.auth("root", "a123456");

    # mongo 登陆测试
    mongo --authenticationDatabase admin -u root -p a123456

##### 7.Rabbitmq相关信息

###### 7.1 访问页面地址

    http://localhost:15672/#/

###### 7.2 账户密码
    
    默认账号密码可以在配置中查看也可以自行更改，默认：guest guest

###### 7.3 自行创建新的管理员

    rabbitmqctl add_user admin a123456
    rabbitmqctl set_permissions -p / admin ".*" ".*" ".*"
    rabbitmqctl set_user_tags admin administrator

##### 8.Nacos
    
    http://localhost:8848/nacos/index.html

##### 9.Elasticsearch
    
    http://localhost:9200/

###### 9.1 初始化es密码

    # 进入容器中
    elasticsearch-setup-passwords interactive

##### 10.Kibana Es Ui管理界面

    http://localhost:5601/